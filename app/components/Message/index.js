import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ClickHoverComp from 'components/ClickHoverComp';

const MessageBody = styled.div `
  padding: 5px;
  margin: 10px 15px;
  position: relative;
  background-color: #FBF6F6;
  border-radius: 3px 3px 3px 3px;
  -moz-border-radius: 3px 3px 3px 3px;
  -webkit-border-radius: 3px 3px 3px 3px;
`;

const Date = styled.div `
  position: absolute;
  right: 3px;
  top: 1px;
  font-size: 0.8em;
`;

const Username = styled.button `
  margin: 0;
  font-size: 1.3em;
  &:hover, &:active {
    color: #ef4a47;
  }
`;

const Conv = styled.button `
  margin: 0;
  color: #fbfbfb;
  background-color: #ef4a47;
  border-radius: 12px 12px 12px 12px;
  -moz-border-radius: 12px 12px 12px 12px;
  -webkit-border-radius: 12px 12px 12px 12px;
`;

const Message = ({ el, idx, priv_msg, enable }) => (
  <ClickHoverComp
    render={({ state, onLeave, onClick, onHover }) => (
      <MessageBody
        state={state}
        key={idx}
        onMouseLeave={() => onLeave()}
        onMouseEnter={() => onHover()}
      >
        <Username onClick={onClick}>{el.user}</Username>
        <div>{el.text}</div>
        <Date>{el.time}</Date>
        <p>{state}</p>
        {state && enable && <Conv onClick={() => priv_msg(el.user)}>
          Write private message
        </Conv>}
      </MessageBody>
  )}
  ></ClickHoverComp>
);

Message.propTypes = {
  idx: PropTypes.number,
  enable: PropTypes.bool,
  el: PropTypes.object,
  priv_msg: PropTypes.func,
};

export default Message;
