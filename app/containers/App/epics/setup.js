import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs/Observable';
import { combineEpics } from 'redux-observable';
import { push } from 'react-router-redux';

import Rx from 'rxjs/Rx';

import {
  REGISTER_USERNAME,
  REGISTER_USERNAME_SUCCESS,
  APP_MOUNTED,
  GET_CHANNELS_LIST,
  NEW_USER_JOINED,
  USER_LEFT_CHAT,
  NEW_USERS_LIST,
} from '../constants';

const SetupEpics = (socket, rootUrl) => {
  const newUserBroadcastEpic = () => Observable
      .fromEvent(socket, 'new_user_bc')
      .map((resp) => {
        console.log('new user resp', resp);
        return ({ type: NEW_USER_JOINED, data: resp });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const usersListReceivedEpic = () => Observable
      .fromEvent(socket, 'users_list')
      .map((resp) => {
        console.log('users_list', resp);
        return ({ type: NEW_USERS_LIST, data: resp });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const userLeftBroadcastEpic = () => Observable
      .fromEvent(socket, 'user_left')
      .map((username) => {
        console.log('new user resp', username);
        return ({ type: USER_LEFT_CHAT, data: username });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const sendRegisterUsername = (action$) => action$
      .ofType(REGISTER_USERNAME)
      .mergeMap((action) =>
        ajax.post(`${rootUrl}/users`, { username: action.username },
          { 'Content-Type': 'application/json' })
        .mergeMap((response) => {
          if (response.response.hasOwnProperty('error')) {
            return Rx.Observable.of({ type: 'ADD_FRIEND_REQUEST_ERROR', data: response.response.error });
          }
          return Rx.Observable.of({ type: REGISTER_USERNAME_SUCCESS, username: response.response.username },
                                     push('/chat'),
                                     { type: GET_CHANNELS_LIST });
        }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const sendNewUser = (action$) => action$
      .ofType(REGISTER_USERNAME)
      .mergeMap((action) => {
        socket.emit('new_user', action.username);
        return Rx.Observable.of({ type: 'NEW_USER_SENT' });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const appMountedEpic = (action$) => action$
      .ofType(APP_MOUNTED)
      .mergeMap(() => Rx.Observable.of(push('/')))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  return combineEpics(
    newUserBroadcastEpic,
    usersListReceivedEpic,
    userLeftBroadcastEpic,
    sendRegisterUsername,
    sendNewUser,
    appMountedEpic,
  );
};

export default SetupEpics;
