/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const DEFAULT_LOCALE = 'en';

export const APP_MOUNTED = 'application/mounted/APP_MOUNTED';

export const SEND_MESSAGE = 'application/send_message/action';

// users
export const NEW_USER_JOINED = 'applications/user/NEW_USER_JOINED';
export const NEW_USERS_LIST = 'applications/user/NEW_USERS_LIST';
export const REGISTER_USERNAME = 'application/user/REGISTER_USERNAME';
export const REGISTER_USERNAME_SUCCESS = 'application/user/REGISTER_USERNAME_SUCCESS';
export const USER_LEFT_CHAT = 'application/user/USER_LEFT_CHAT';


// Typing
export const USER_TYPING = 'application/user/USER_TYPING';
export const USER_STOPPED_TYPING = 'application/user/USER_STOPPED_TYPING';
export const SOMEONE_TYPING = 'application/user/SOMEONE_TYPING';
export const SOMEONE_STOPPED_TYPING = 'application/user/SOMEONE_STOPPED_TYPING';

// Channels
export const NEW_CHANNELS_LIST = 'application/channels/NEW_CHANNELS_LIST';
export const NEW_PRIV_CHANNEL = 'application/channels/NEW_PRIV_CHANNEL';
export const OPEN_NEW_CHANNEL = 'application/channels/OPEN_NEW_CHANNEL';
export const OPEN_PRIVATE_CHANNEL = 'application/channels/OPEN_PRIVATE_CHANNEL';
export const CLOSE_CHANNEL = 'application/channels/CLOSE_CHANNEL';
export const CREATE_NEW_CHANNEL = 'application/channels/CREATE_NEW_CHANNEL';
export const CREATE_NEW_PRIVATE_CHANNEL = 'application/channels/CREATE_NEW_PRIVATE_CHANNEL';
export const CHANGE_CHANNEL = 'application/channels/CHANGE_CHANNEL';
export const CHANNELS_ERROR = 'application/channels/CHANNELS_ERROR';
export const REGISTER_CHANNEL_SUCCESS = 'application/channels/REGISTER_CHANNEL_SUCCESS';
export const GET_CHANNELS_LIST = 'application/channels/GET_CHANNELS_LIST';
