import { fromJS } from 'immutable';

import {
  REGISTER_USERNAME_SUCCESS,
  APP_MOUNTED,
  SOMEONE_TYPING,
  SOMEONE_STOPPED_TYPING,
  NEW_CHANNELS_LIST,
  OPEN_NEW_CHANNEL,
  CLOSE_CHANNEL,
  NEW_USER_JOINED,
  USER_LEFT_CHAT,
  NEW_PRIV_CHANNEL,
} from './constants';

const initialState = fromJS({
  user: {
    username: '',
    previous: '',
  },
  current_channel: 'none',
  users_list: [],
  channels_list: [],
  private_channels: [],
  typing_list: [],
  msgs: [],
});

function appReducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER_USERNAME_SUCCESS: {
      localStorage.setItem('previous', action.username);
      return state
        .setIn(['user', 'username'], action.username);
    }
    case APP_MOUNTED: {
      const previous = localStorage.getItem('previous');
      if (previous) {
        return state
          .setIn(['user', 'previous'], previous);
      }
      return state;
    }
    case 'NEW_MESSAGE': {
      return state
        .update('msgs', (list) => list.push(action.payload))
        .update('typing_list', (list) => list.filter((e) => e !== action.payload.user));
    }
    case 'USERS_LIST':
      return state
        .set('users_list', fromJS(action.payload));
    case SOMEONE_TYPING: {
      const username = state.getIn(['user', 'username']);
      if (username !== action.payload) {
        return state
          .update('typing_list', (list) => list.push(action.payload));
      }
      return state;
    }
    case SOMEONE_STOPPED_TYPING: {
      return state
        .update('typing_list', (list) => list.filter((e) => e !== action.payload));
    }
    case NEW_CHANNELS_LIST: {
      return state
        .set('channels_list', fromJS(action.payload.map((e) => {
          e.open = e.name === 'trash';
          return e;
        })))
        .set('current_channel', action.payload.filter((e) => e.name === 'trash')[0]._id);
    }
    case OPEN_NEW_CHANNEL: {
      const index = state.get('channels_list').toJS().findIndex((e) => e._id === action.id);
      return state
        .setIn(['channels_list', index, 'open'], true)
        .set('current_channel', state.get('channels_list').toJS()[index]._id);
    }
    case CLOSE_CHANNEL: {
      const index = state.get('channels_list').toJS().findIndex((e) => e._id === action.id);
      return state
        .setIn(['channels_list', index, 'open'], false)
        .set('current_channel', state.get('channels_list').toJS()[0]._id)
        .update('msgs', (list) => list.filter((e) => action.id !== e.channelID));
    }
    case NEW_USER_JOINED: {
      return state
        .update('users_list', (list) => list.push(action.data));
    }
    case USER_LEFT_CHAT: {
      return state
        .update('users_list', (list) => list.filter((e) => e.username !== action.data));
    }
    case NEW_PRIV_CHANNEL: {
      return state
        .update('channels_list', (list) => list.push(fromJS(action.data)));
    }
    default:
      return state;
  }
}

export default appReducer;
