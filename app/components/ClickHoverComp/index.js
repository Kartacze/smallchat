import React from 'react';

import { setObservableConfig, componentFromStream, createEventHandler } from 'recompose';
import rxjsConfig from 'recompose/rxjsObservableConfig';

import { Observable } from 'rxjs';
import rxjs from 'rxjs';

setObservableConfig(rxjsConfig);

const RenderHolder = ({ state, onLeave, onClick, onHover, render, ...props }) =>
render({ state, onLeave, onHover, onClick, ...props });

const ClickHoverComp = componentFromStream((props$) => {
  const { stream: onLeave$, handler: onLeave } = createEventHandler();
  const { stream: onClick$, handler: onClick } = createEventHandler();
  const { stream: onHover$, handler: onHover } = createEventHandler();

  const active$ = onHover$
    .bufferToggle(onLeave$.startWith('1'), () => onLeave$.delay(1000))
    .map((e) => e.length)
    .filter((e) => e === 1);

  const state$ = Observable.merge(
    onClick$.mapTo(true),
    active$.mapTo(false)
  ).startWith(false);

  return props$
    .combineLatest(state$, (props, state) => ({ ...props, state, onLeave, onClick, onHover }))
    .map(RenderHolder);
});

export default ClickHoverComp;
