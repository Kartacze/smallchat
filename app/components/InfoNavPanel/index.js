import React from 'react';
import styled from 'styled-components';

import MdMoreVert from 'react-icons/lib/md/more-vert';
import MdArrowBack from 'react-icons/lib/md/arrow-back';

const Body = styled.div`
  display: flex;
  align-items: center;
  flex: none;
  position: relative;
  -webkit-order: 1;
  order: 1;
  height: 50px;
  background-color: #AE109B;
`;

const BackButton = styled.div`
  flex: 0 1 50px;
`;

const InfoPanel = styled.div`
  background-color: blue;
  flex: 1 1 0;
`;

const MoreInfoPanel = styled.div`
  flex: 0 1 50px;
`;

const NotiDiv = styled.div`
  flex: 0 1 50px;
  margin: auto;
  text-align: center;
`;

const Noti = styled.div`
  margin: auto;  
`;

const InfoNavPanel = ({ mobile, notifications }) => (
  <Body>
    {mobile && <BackButton> <MdArrowBack style={{ display: 'block', margin: 'auto' }} size={35}> </MdArrowBack></BackButton> }
    <InfoPanel />
    {notifications && <NotiDiv><Noti>{notifications}</Noti></NotiDiv>}
    <MoreInfoPanel ><MdMoreVert size={30} style={{ display: 'block', margin: 'auto' }} />
    </MoreInfoPanel>
  </Body>
);

export default InfoNavPanel;
