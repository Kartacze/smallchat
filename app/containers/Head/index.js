import React from 'react';

import styled from 'styled-components';


const Box = styled.nav`
  -webkit-flex: none;
  flex: none;
  width: 100%;
  height: 70px;
  box-sizing: border-box;
  display: block;
  z-index: 1;
  order: 1;
  padding: 5px;
  background-color: #620957;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
  color: #fbfbfb;
`;

// class Head extends React.Component {
//   render() {
//     return (
//       <Footer>
//         <button>back</button>
//         <p> Hello</p>
//       </Footer>
//     );
//   }
// }

const Head = () => (
  <Box>
    <button>back</button>
    <p> Hello</p>
  </Box>
);

export default Head;
