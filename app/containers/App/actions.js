import {
  SEND_MESSAGE,
  REGISTER_USERNAME,
  APP_MOUNTED,
  USER_TYPING,
  CREATE_NEW_CHANNEL,
  OPEN_NEW_CHANNEL,
  CLOSE_CHANNEL,
  OPEN_PRIVATE_CHANNEL,
} from './constants';


export function sendMessageAction(msg) {
  return {
    type: SEND_MESSAGE,
    msg,
  };
}

export function sendRegisterUsername(username) {
  return {
    type: REGISTER_USERNAME,
    username,
  };
}

export function componentMountedAction() {
  return {
    type: APP_MOUNTED,
  };
}

export function userTypingAction() {
  return {
    type: USER_TYPING,
  };
}

export function createNewChannelAction(data) {
  return {
    type: CREATE_NEW_CHANNEL,
    data,
  };
}

export function openChannelAction(id) {
  return {
    type: OPEN_NEW_CHANNEL,
    id,
  };
}

export function closeChannelAction(id) {
  return {
    type: CLOSE_CHANNEL,
    id,
  };
}
