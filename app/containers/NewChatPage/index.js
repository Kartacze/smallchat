import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import styled from 'styled-components';

import {
  createNewChannelAction,
} from 'containers/App/actions';

import {
  makeSelectUsername,
  makeSelectUsersList,
  selectChannelsList,
 } from 'containers/App/selectors';

const RegForm = styled.div`
  padding: 5px 0 5px;
  margin: 30px auto;
  width: 300px;
  min-height: 100px;
  color: #620957;
  background-color: #ef4a47;
  text-align: center;
  padding-top: 5px;
  border-radius: 5px 5px 5px 5px;
  -moz-border-radius: 5px 5px 5px 5px;
  -webkit-border-radius: 5px 5px 5px 5px;
`;

const StyledButton = styled.input`
  width: 100%;
  border-bottom: 2px solid #EBE2EA;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;

const Input = styled.input`
  position: relative;
  width: 100%;
  padding: 7px;
  margin-left 10px;
  margin-right 10px;
  border-bottom: 2px solid #EBE2EA;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;

class NewChatPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.createNewChannel({ name: this.state.value, private: false, between: [] });
  }

  render() {
    return (
      <RegForm>
        <form onSubmit={this.handleSubmit} style={{ height: '100%' }}>
          <Input placeholder="Create new Channel" type="text" value={this.state.value} onChange={this.handleChange} />
          <StyledButton type="submit" value="Submit" />
        </form>
      </RegForm>
    );
  }
}

NewChatPage.propTypes = {
  createNewChannel: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    createNewChannel: (name) => dispatch(createNewChannelAction(name)),
  };
}

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
  users_list: makeSelectUsersList(),
  channels_list: selectChannelsList(),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewChatPage);
