import React from 'react';
import styled from 'styled-components';

const ChannelBody = styled.div`
  position: relative;
`;

const ChannelHeader = styled.div`
  position: relative;
  font-weight: bold;
  font-size: 20px;
`;

const ChannelButton = styled.button`
  font-size: 25px;
  display: inline;
  float: right;
`;

const ItemBody = styled.div`
  margin: 0;
  padding: 0;
  display: block;
  position: relative;

`;

const CancBut = styled.button`
  position: absolute;
  right: 0;

`;

const Item = ({id, name, click, open, close }) =>
  (<ItemBody>
    <button onClick={() => click(id)}> {name} </button>
    {open && <CancBut onClick={() => close(id)}> x </CancBut>}
  </ItemBody>)
  ;

const Channel = ({ list, check, newChannel, closeChannel }) => {
  const items = list.map((e) => <Item id={e._id} name={e.name} click={check} open={e.open} close={closeChannel} />);
  return (
    <ChannelBody>
      <ChannelHeader> Channels
      <ChannelButton onClick={newChannel}> + </ChannelButton>
      </ChannelHeader>
      { items }
    </ChannelBody>
  );
};

export default Channel;
