import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Switch, Route } from 'react-router-dom';

import ChatPage from 'components/ChatPage';
import NewChatPage from 'containers/NewChatPage';
import Register from 'containers/Register';

import {
  componentMountedAction,
} from '../App/actions';

const Messanger = () =>
  (<Switch>
    <Route exact path="/" component={Register} />
    <Route path="/chat" component={ChatPage} />
    <Route path="/newchannel" component={NewChatPage} />
  </Switch>);

Messanger.propTypes = {
  componentMounted: PropTypes.func,
};

export function mapDispatchToProps(dispatch) {
  return {
    componentMounted: () => dispatch(componentMountedAction()),
  };
}


export default withRouter(connect(null, mapDispatchToProps)(Messanger));
