import styled from 'styled-components';

const BoxLeft = styled.div`
margin: 0;
padding: 0;
`;

const BoxRight = styled.div`
margin: 0;
padding: 0;
`;

const BodyBox = styled.div`
  margin: 0;
  padding: 0;
  height: 100vh;
  width: 100vw;
  z-index: 0;
  display: flex;

  ${BoxLeft} {
    display: none;
  }

  ${BoxRight} {
    height: 100%;
    width: 100%;
  }

  &:after {
    content: " ";
    position: absolute;
    left: 0;
    top: 0;
    height: 20vh;
    width: 100%;
    background-color: #620957;
    z-index: -1;
  }

  @media (min-width: 768px) {
    ${BoxLeft} {
      height: 100%;
      width: 30%;
      display: inline;
    }
    ${BoxRight} {
      height: 100%;
      width: 70%;
    }
  }

  @media (min-width: 1025px) {
    height: 85vh;
    width: 80vw;
  }

  @media (min-width: 768px) and (max-width: 1024px) and (orientation: landscape) {
    height: 90vh;
    width: 90vw;
  }

  @media (min-width: 768px) and (max-width: 1024px) {
    height: 80vh;
    width: 90vw;
  }
`;

export { BodyBox, BoxLeft, BoxRight };
