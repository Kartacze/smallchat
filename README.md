# SmallChat

Simple chat implementation

## Getting Started

This project is still work in progress.
[Live demo](http://chat.ted.usermd.net)

TODO

## Running the tests

TODO

## Deployment

TODO

## Built With

* [React](https://reactjs.org/) - UI Library
* [Redux](https://redux.js.org/) - State container
* [redux-observable](https://github.com/redux-observable/redux-observable) - Redux middleware
* [Socket.io](https://github.com/socketio/socket.io) - realtime stuff
* [Babel](https://babeljs.io/) - compiler
* [webpack](https://github.com/webpack/webpack) - Bundler

## Authors

* **Teodor Pytka** - *Initial work* - [kartacze](https://bitbucket.org/Kartacze)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* This project is based on [react-boilerplate](https://github.com/react-boilerplate/react-boilerplate)
* Inspiration - [react-redux-socketio-chat](https://github.com/raineroviir/react-redux-socketio-chat)

## TODO

[ ] add some RWD
[ ] remove react router - ?
[ ] add header in mobile mode ? maybe also in normal mode ?
[ ] handle new channel validation
[ ] handle errors while loading 
[ ] handle error while leaving
