import { createSelector } from 'reselect';

const selectRoute = (state) => state.get('global');

const makeSelectMessages = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('msgs').toJS().filter((e) => e.channelID === routeState.get('current_channel'))
);

const makeSelectUsername = () => createSelector(
  selectRoute,
  (routeState) => routeState.getIn(['user', 'username'])
);

const makeSelectUser = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('user').toJS()
);

const makeSelectUsersList = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('users_list').toJS()
);

const selectTypingList = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('typing_list').toJS()
);

const selectChannelsList = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('channels_list').toJS()
);

const selectCurrentChannel = () => createSelector(
  selectRoute,
  (routeState) => routeState.get('current_channel')
);

const selectUserSocket = (username) => createSelector(
  selectRoute,
  (routeState) => routeState.get('users_list').toJS().filter((e) => e.username === username)[0].id
);


export {
  makeSelectMessages,
  makeSelectUsername,
  makeSelectUser,
  makeSelectUsersList,
  selectTypingList,
  selectChannelsList,
  selectCurrentChannel,
  selectUserSocket,
};
