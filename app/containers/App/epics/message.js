import { Observable } from 'rxjs/Observable';
import { combineEpics } from 'redux-observable';

import Rx from 'rxjs/Rx';

import {
  SEND_MESSAGE,
  USER_STOPPED_TYPING,
} from '../constants';

const messageEpics = (socket) => {
  const newMessage = () => Observable
      .fromEvent(socket, 'msg')
      .map((payload) => ({ type: 'NEW_MESSAGE', payload }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const usersListIOmessage = (actions) => Observable
      .fromEvent(socket, 'users_list')
      .map((payload) => ({ type: 'USERS_LIST', payload }))
      .takeUntil(actions.ofType('STOP_SOCKET'))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const sendMessage = (action$) => action$
      .ofType(SEND_MESSAGE)
      .mergeMap((action) => {
        socket.emit('new_msg', action.msg);
        return Rx.Observable.of({ type: 'MESSAGE_SENT' }, { type: USER_STOPPED_TYPING });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  return combineEpics(
    newMessage,
    usersListIOmessage,
    sendMessage,
  );
};

export default messageEpics;
