import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import styled from 'styled-components';

import {
  sendMessageAction,
  userTypingAction,
} from 'containers/App/actions';

import {
  makeSelectUsername,
  selectCurrentChannel,
 } from 'containers/App/selectors';


const Footer = styled.footer`
  -webkit-flex: none;
  flex: none;
  width: 100%;
  box-sizing: border-box;
  display: block;
  z-index: 1;
  order: 3;
  padding: 5px;
  background-color: #620957;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
  color: #fbfbfb;
`;

const Input = styled.input`
  position: relative;
  width: 100%;
  padding: 7px;
  margin-right 10px;
  border-bottom: 2px solid #ef4a47;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;

const StyledButton = styled.input`
  width: 100%;
  border-bottom: 2px solid #ef4a47;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;

class Writer extends React.Component {
  constructor() {
    super();
    this.state = { value: '' };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
    this.props.userTyping();
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.state.value.length !== 0) {
      const msg = {
        text: this.state.value,
        channelID: this.props.curChannel,
        user: this.props.username,
        time: (new Date()).toISOString().replace(/T|\.\d\d\dZ$/g, ' '),
      };
      this.props.onMessageSend(msg);
      this.setState({ value: '' });
    }
  }

  render() {
    return (
      <Footer>
        <form onSubmit={this.handleSubmit} style={{ height: '100%' }}>
          <Input placeholder="Napisz wiadomość" type="text" value={this.state.value} onChange={this.handleChange} />
          <StyledButton type="submit" value="Send" />
        </form>
      </Footer>
    );
  }
}

Writer.propTypes = {
  userTyping: PropTypes.func,
  onMessageSend: PropTypes.func,
  curChannel: PropTypes.string,
  username: PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
  return {
    onMessageSend: (e) => dispatch(sendMessageAction(e)),
    userTyping: () => dispatch(userTypingAction()),
  };
}

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
  curChannel: selectCurrentChannel(),
});

export default connect(mapStateToProps, mapDispatchToProps)(Writer);
