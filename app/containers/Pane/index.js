import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import {
  makeSelectMessages,
  selectTypingList,
  selectCurrentChannel,
  makeSelectUsername,
 } from 'containers/App/selectors';

import {
 createNewChannelAction,
} from 'containers/App/actions';

import Message from 'components/Message';
import styled from 'styled-components';


const Msgs = styled.div`
  -webkit-flex: 1 1 0;
  flex: 1 1 0;
  position: relative;
  -webkit-order: 2;
  order: 2;
  overflow-y: scroll;
  overflow-x: hidden;
`;

const TypingDiv = styled.div`
  margin: 0;
  padding: 0;
  position: relative;
  right: 0;
  bottom: 0;
`;

const TypingP = styled.p`
  margin: 0;
  padding: 0 0 0 4px;
`;

class Pane extends React.Component {
  componentDidUpdate() {
    this.scrollToBottom();
  }

  scrollToBottom = () => {
    const node = ReactDOM.findDOMNode(this.messagesEnd);
    node.scrollIntoView({ block: 'end', behavior: 'smooth' });
  }

  handleNewPrivChannel(username) {
    this.props.createNewPrivChannel({ private: true,
      name: `${username}+${this.props.username}`,
      between: [username, this.props.username] });
  }

  render() {
    const messages = this.props.msgs.map((e, i) =>
      (<Message
        el={e}
        idx={i}
        enable={e.user !== this.props.username}
        priv_msg={(el) => this.handleNewPrivChannel(el)}
      />)
    );

    return (
      <Msgs>
        {messages}
        <TypingDiv> { this.props.typing_list.length !== 0 &&
          this.props.typing_list.map((e) => <TypingP>/{e}/ cos tworzy</TypingP>)}
        </TypingDiv>
        <div ref={(el) => { this.messagesEnd = el; }} />
      </Msgs>
    );
  }
}

Pane.propTypes = {
  createNewPrivChannel: PropTypes.func,
  typing_list: PropTypes.object,
  msgs: PropTypes.array,
  username: PropTypes.string,
};

export function mapDispatchToProps(dispatch) {
  return {
    createNewPrivChannel: (name) => dispatch(createNewChannelAction(name)),
  };
}

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
  msgs: makeSelectMessages(),
  currentChannel: selectCurrentChannel(),
  typing_list: selectTypingList(),
});

export default connect(mapStateToProps, mapDispatchToProps)(Pane);
