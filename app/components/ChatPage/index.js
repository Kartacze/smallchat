import React from 'react';
import styled from 'styled-components';

import Pane from 'containers/Pane';
import Writer from 'containers/Writer';
import InfoNavPanel from 'containers/InfoNavPanel';

const Body = styled.div`
  display: flex;
  flex-direction: column;
  overflow: hidden;
  width: 100%;
  height: 100vh;
`;

const ChatPage = () =>
  (<Body>
    <InfoNavPanel />
    <Pane />
    <Writer />
  </Body>);


export default ChatPage;
