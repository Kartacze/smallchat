import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';

import styled from 'styled-components';

import { makeSelectUser } from 'containers/App/selectors';
import { sendRegisterUsername } from 'containers/App/actions';

import Loader from './Loader';

const RegForm = styled.div`
  padding: 5px 0 5px;
  margin: 30px auto;
  width: 300px;
  min-height: 100px;
  color: #620957;
  background-color: #ef4a47;
  text-align: center;
  padding-top: 5px;
  border-radius: 5px 5px 5px 5px;
  -moz-border-radius: 5px 5px 5px 5px;
  -webkit-border-radius: 5px 5px 5px 5px;
`;

const StyledButton = styled.input`
  width: 100%;
  border-bottom: 2px solid #EBE2EA;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;

const Input = styled.input`
  position: relative;
  width: 100%;
  padding: 7px;
  margin-right 10px;
  border-bottom: 2px solid #EBE2EA;
  border-radius: 0px 0px 5px 5px;
  -moz-border-radius: 0px 0px 5px 5px;
  -webkit-border-radius: 0px 0px 5px 5px;
`;


class Register extends React.Component {
  constructor() {
    super();
    this.state = { value: '', loading: false, error: '' };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    event.preventDefault();
    if (this.validateUsername(this.state.value)) {
      this.props.onRegisterUsername(this.state.value);
      this.setState({ loading: true });
    }
  }

  handlePrevious() {
    this.props.onRegisterUsername(this.props.user.previous);
    this.setState({ loading: true });
  }

  validateUsername(username) {
    if (username.length < 4) {
      this.setState({ error: 'username too short, must have more than 3 characters' });
      return false;
    }
    if (username.length > 10) {
      this.setState({ error: 'username too long, must have less than 10 characters' });
      return false;
    }
    if (!username.match(/^[^\\\/&]*$/)) {
      this.setState({ error: 'using not allowed characters' });
      return false;
    }
    return true;
  }

  render() {
    return (
      <RegForm>
        { this.state.loading && <Loader /> ||
        <form onSubmit={this.handleSubmit} style={{ height: '100%' }}>
          <Input placeholder="Podaj nazwę użytkownika" type="text" value={this.state.value} onChange={this.handleChange} />
          {this.state.error && <p> {this.state.error} </p>}
          <StyledButton type="submit" value="Wyślij" />
          { this.props.user.previous &&
            <StyledButton type="button" value={`Poprzedni: ${this.props.user.previous}`} onClick={() => this.handlePrevious()} /> }
        </form>
        }
      </RegForm>
    );
  }
}

Register.propTypes = {
  onRegisterUsername: PropTypes.func,
  user: PropTypes.object,
};

export function mapDispatchToProps(dispatch) {
  return {
    onRegisterUsername: (e) => dispatch(sendRegisterUsername(e)),
  };
}

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
