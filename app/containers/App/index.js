import React from 'react';

import SideBar from 'containers/SideBar';
import Messanger from 'containers/Messanger';

import { BodyBox, BoxLeft, BoxRight } from './BodyBox';
import { Body } from './Body';
import { AppWrapper } from './AppWrapper';

const App = () =>
  (<AppWrapper>
    <BodyBox>
      <BoxLeft>
        <SideBar />
      </BoxLeft>
      <BoxRight>
        <Body>
          <Messanger />
        </Body>
      </BoxRight>
    </BodyBox>
  </AppWrapper>);

export default App;
