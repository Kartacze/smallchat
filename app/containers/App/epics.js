import { combineEpics } from 'redux-observable';
import io from 'socket.io-client';

import ChannelEpics from './epics/channel';
import MessageEpics from './epics/message';
import TypingEpics from './epics/typing';
import SetupEpics from './epics/setup';

const rootUrl = 'http://192.168.0.107:3090';
const socket = io('192.168.0.107:3090/');

export const rootEpic = combineEpics(
  ChannelEpics(socket, rootUrl),
  MessageEpics(socket),
  TypingEpics(socket),
  SetupEpics(socket, rootUrl),
);
