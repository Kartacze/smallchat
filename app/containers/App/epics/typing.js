import { Observable } from 'rxjs/Observable';
import { combineEpics } from 'redux-observable';

import Rx from 'rxjs/Rx';

import {
  USER_TYPING,
  USER_STOPPED_TYPING,
  SOMEONE_TYPING,
  SOMEONE_STOPPED_TYPING,
} from '../constants';

const typingEpics = (socket) => {
  const watchUserStopTyping = (action$) => action$
      .ofType(USER_TYPING)
      .debounceTime(2500)
      .mergeMap(() => {
        socket.emit('stop_typing', 'user');
        return Rx.Observable.of({ type: USER_STOPPED_TYPING });
      });

  const watchUserTyping = (action$) => action$
    .ofType(USER_TYPING)
    .throttle(() => action$.ofType(USER_STOPPED_TYPING))
    .mergeMap(() => {
      socket.emit('typing', 'user');
      return Rx.Observable.of({ type: 'USER IS TYPING RIGHT NOW' });
    });

  const typingBroadcastEpic = () => Observable
      .fromEvent(socket, 'typing_bc')
      .map((payload) => ({ type: SOMEONE_TYPING, payload }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const stoppedTypingBroadcastEpic = () => Observable
      .fromEvent(socket, 'stop_typing_bc')
      .map((payload) => ({ type: SOMEONE_STOPPED_TYPING, payload }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  return combineEpics(
    watchUserStopTyping,
    watchUserTyping,
    typingBroadcastEpic,
    stoppedTypingBroadcastEpic,
  );
};

export default typingEpics;
