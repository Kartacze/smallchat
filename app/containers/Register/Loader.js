import styled, { keyframes } from 'styled-components';

const fadeIn = keyframes`
  from {
    transform: rotate(0deg);
  }

  to {
    transform: rotate(360deg);
  }
`;

const Loader = styled.div`
  border: 5px solid #f3f3f3; /* Light grey */
  border-top: 5px solid #3498db; /* Blue */
  border-radius: 50%;
  width: 60px;
  height: 60px;
  animation: ${fadeIn} 2s linear infinite;
  margin: 0 auto;
`;

export default Loader;
