import styled from 'styled-components';

const Body = styled.div`

  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: column;
  flex-direction: column;
  overflow: hidden;
  margin: 0;
  border-radius: 5px 5px 5px 5px;
  -moz-border-radius: 5px 5px 5px 5px;
  -webkit-border-radius: 5px 5px 5px 5px;
  -webkit-box-shadow: 5px 5px 5px 0px rgba(8,10,9,0.6);
  -moz-box-shadow: 5px 5px 5px 0px rgba(8,10,9,0.4);
  box-shadow: 5px 5px 5px 0px rgba(8,10,9,0.4);
  background-color: #cb8caf;

  height: 100%;

  @media only screen and (min-width: 768px) {
    width: 100%;
  }
`;

export { Body };
