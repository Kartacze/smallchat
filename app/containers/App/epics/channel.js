import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs/Observable';
import { combineEpics } from 'redux-observable';
import { push } from 'react-router-redux';

import Rx from 'rxjs/Rx';

import {
  NEW_CHANNELS_LIST,
  CHANNELS_ERROR,
  CREATE_NEW_CHANNEL,
  OPEN_NEW_CHANNEL,
  CLOSE_CHANNEL,
  GET_CHANNELS_LIST,
  NEW_PRIV_CHANNEL,
} from '../constants';

import {
  selectUserSocket,
} from '../selectors';

const channelEpics = (socket, rootUrl) => {
  const newChannelBroadcastEpic = () => Observable
      .fromEvent(socket, 'new_channel_bc')
      .map(() => ({ type: GET_CHANNELS_LIST }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const newPrivChannelBroadcastEpic = () => Observable
      .fromEvent(socket, 'new_priv_channel_bc')
      .map((data) => {
        socket.emit('subscribe', data._id);
        return ({ type: NEW_PRIV_CHANNEL, data });
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  // when the user will be registered fetch the Channels
  const fetchChannels = (action$) => action$
      .ofType(GET_CHANNELS_LIST)
      .mergeMap(() =>
        ajax.get(`${rootUrl}/channels`, { 'Content-Type': 'application/json' })
        .map((response) => {
          if (response.response.hasOwnProperty('error')) {
            return ({ type: CHANNELS_ERROR, data: response.response.error });
          }
          return ({ type: NEW_CHANNELS_LIST, payload: response.response });
        }));

  const createNewChannelEpic = (action$, store) => action$
      .ofType(CREATE_NEW_CHANNEL)
      .mergeMap((action) =>
        ajax.post(`${rootUrl}/channels`, { name: action.data.name,
          private: action.data.private,
          between: action.data.between },
          { 'Content-Type': 'application/json' })
        .mergeMap((response) => {
          if (response.response.hasOwnProperty('error')) {
            return Rx.Observable.of({ type: 'ADD_FRIEND_REQUEST_ERROR', data: response.response.error });
          }
          if (action.data.private) {
            const socketID = selectUserSocket(action.data.between[0])(store.getState());
            // console.log('new priv chanenl', socketID, response.response );
            socket.emit('new_priv_channel', { socketID, channel: response.response });
            socket.emit('subscribe', response.response._id);
            return Rx.Observable.of({ type: NEW_PRIV_CHANNEL, data: response.response }, push('/chat'));
          } else {
            console.log('new chanenl', response.response );
            socket.emit('new_channel', response.response);
            return Rx.Observable.of({ type: GET_CHANNELS_LIST }, push('/chat'));
          }
        }))
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const sendChannelSubscribeEpic = (action$) => action$
      .ofType(OPEN_NEW_CHANNEL)
      .mergeMap((action) => {
        socket.emit('subscribe', action.id);
        return Rx.Observable.of({ type: 'NEW_USER_SENT' }); // to be changed
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  const sendChannelUnsubscribeEpic = (action$) => action$
      .ofType(CLOSE_CHANNEL)
      .mergeMap((action) => {
        socket.emit('unsubscribe', action.id);
        return Rx.Observable.of({ type: 'NEW_USER_SENT' }); // to be changed
      })
      .catch((err) => Observable.of({ type: 'ERROR', data: err }));

  // const privateChatEpic = (action$, store) => action$
  //     .ofType(OPEN_PRIVATE_CHANNEL)
  //     .map((response) => {
  //       const channelsList = selectChannelsList()(store.getState());
  //       const username = makeSelectUsername()(store.getState());
  //       if (channelsList.filter((e) => e.private === true).filter((e) => response.username in e.between)[0]) {
  //         // then only change channel
  //         return ({ type: 'NEW_USER_SENT' });
  //       }
  //         // first create private channel
  //       return ({ type: CREATE_NEW_CHANNEL,
  //         data: { private: true,
  //           between: [username, response.username],
  //           name: `${username}+${response.username}` },
  //       });
  //     })
  //     .catch((err) => Observable.of({ type: 'ERROR', data: err }));
  // const createPrivateChannelEpic = (action$, store) => {
  //   return action$
  //     .ofType(CREATE_NEW_PRIVATE_CHANNEL)
  //     .mergeMap( response => Rx.Observable.of(push('/')))
  //     .catch((err) => {
  //       console.log('message sending error', err);
  //       return Observable.of({type: 'ERROR'});
  //     });
  // }

  return combineEpics(
    newChannelBroadcastEpic,
    newPrivChannelBroadcastEpic,
    channelEpics,
    fetchChannels,
    createNewChannelEpic,
    sendChannelSubscribeEpic,
    sendChannelUnsubscribeEpic,
    // privateChatEpic,
  );
};

export default channelEpics;
