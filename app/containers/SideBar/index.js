import React from 'react';
import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

import styled from 'styled-components';

import { sendMessageAction,
  componentMountedAction,
  openChannelAction,
  closeChannelAction,
} from 'containers/App/actions';

import {
  makeSelectUsername,
  makeSelectUsersList,
  selectChannelsList,
 } from 'containers/App/selectors';

import Channel from 'components/Channel';


const Body = styled.div`
  margin-right: 5%;
  padding: 8px;
  max-height: 100%;
  background-color: #ef4a47;
  border-radius: 5px 5px 5px 5px;
  -moz-border-radius: 5px 5px 5px 5px;
  -webkit-border-radius: 5px 5px 5px 5px;
`;


class SideBar extends React.Component {
  handleClick(id) {
    this.props.openChannel(id);
  }

  handleCloseChannel(id) {
    this.props.closeChannel(id);
  }

  render() {
    return (
      <Body>
        <Channel
          list={this.props.channels_list}
          check={(e) => this.handleClick(e)}
          newChannel={this.props.createChannel}
          closeChannel={(e) => this.handleCloseChannel(e)}
        />
        {
          this.props.users_list.length !== 0 &&
          <div>
            <h3> Users </h3>
              {this.props.users_list.map((e) => <div> { e.username } </div>)}
          </div>
        }
      </Body>
    );
  }
}

SideBar.propTypes = {
  openChannel: PropTypes.func,
  closeChannel: PropTypes.func,
  createChannel: PropTypes.func,
  channels_list: PropTypes.array,
  users_list: PropTypes.array,
};

export function mapDispatchToProps(dispatch) {
  return {
    onMessageSend: (e) => dispatch(sendMessageAction(e)),
    componentMounted: () => dispatch(componentMountedAction()),
    openChannel: (id) => dispatch(openChannelAction(id)),
    closeChannel: (id) => dispatch(closeChannelAction(id)),
    createChannel: () => dispatch(push('/newchannel')),
    newChannel: () => dispatch(push('/chat')),
  };
}

const mapStateToProps = createStructuredSelector({
  username: makeSelectUsername(),
  users_list: makeSelectUsersList(),
  channels_list: selectChannelsList(),
});

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);
